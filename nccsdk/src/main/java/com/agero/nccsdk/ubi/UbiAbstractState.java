package com.agero.nccsdk.ubi;

import android.content.Context;

/**
 * Created by james hermida on 9/7/17.
 */

abstract class UbiAbstractState implements UbiState {

    final Context applicationContext;

    UbiAbstractState(Context context) {
        this.applicationContext = context;
    }
}
