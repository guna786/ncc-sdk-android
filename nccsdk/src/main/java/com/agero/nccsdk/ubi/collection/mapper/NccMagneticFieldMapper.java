package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMagneticFieldData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccMagneticFieldMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccMagneticFieldData mfd = (NccMagneticFieldData) data;
        clearStringBuilder();
        sb.append("M,");                                // Code
        sb.append(mfd.getBiasX()); sb.append(",");      // Bx
        sb.append(mfd.getBiasY()); sb.append(",");      // By
        sb.append(mfd.getBiasZ()); sb.append(",");      // Bz
        sb.append(mfd.getAccuracy()); sb.append(",");   // Baccuracy
        sb.append("NA,NA,NA,NA,NA,");
        sb.append(mfd.getTimestamp()); sb.append(",");  // UTCTime
        sb.append(getReadableTimestamp(mfd.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
