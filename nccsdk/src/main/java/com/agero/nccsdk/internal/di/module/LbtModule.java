package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.di.qualifier.LbtDataPath;
import com.agero.nccsdk.internal.di.qualifier.LbtKinesisClient;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesParentDirectory;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesPath;
import com.agero.nccsdk.lbt.LbtInactiveState;
import com.agero.nccsdk.lbt.LbtState;
import com.agero.nccsdk.lbt.LbtStateMachine;
import com.agero.nccsdk.lbt.network.LbtDataManager;
import com.agero.nccsdk.lbt.network.LbtSyncManager;
import com.agero.nccsdk.lbt.util.KinesisUploadTimedOperation;
import com.agero.nccsdk.lbt.util.TimedOperation;
import com.amazonaws.auth.AWSCredentialsProvider;

import java.io.File;
import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LbtModule {

    /**
     * Real-time data directory
     */
    private final String REAL_TIME_DATA_DIRECTORY = "rt";

    @Provides
    @Singleton
    StateMachine<LbtState> provideLbtStateMachine() {
        return new LbtStateMachine(new LbtInactiveState());
    }

    @Provides
    TimedOperation provideTimedOperation(@LbtKinesisClient KinesisClient sensorKinesisClient) {
        return new KinesisUploadTimedOperation(sensorKinesisClient);
    }

    @Provides
    @Singleton
    LbtSyncManager provideLbtSyncManager(LbtDataManager lbtDataManager) {
        return lbtDataManager;
    }

    @Provides
    @Singleton
    @com.agero.nccsdk.internal.di.qualifier.LbtKinesisClient
    KinesisClient provideLbtStreamsKinesisClient(Context context, Executor executor, @LbtDataPath File directory, AWSCredentialsProvider cognitoCachingCredentialsProvider, AuthManager authManager) {
        return new com.agero.nccsdk.internal.data.network.aws.kinesis.LbtKinesisClient(context, executor, directory, cognitoCachingCredentialsProvider, authManager);
    }

    @Provides
    @Singleton
    @LbtDataPath
    File provideRealTimeDataPath(@SdkLocalFilesPath String sdkLocalFilesPath, @SdkLocalFilesParentDirectory String sdkLocalFilesParentDirectory) {
        return new File(sdkLocalFilesPath + File.separator + sdkLocalFilesParentDirectory + File.separator + REAL_TIME_DATA_DIRECTORY);
    }
}
