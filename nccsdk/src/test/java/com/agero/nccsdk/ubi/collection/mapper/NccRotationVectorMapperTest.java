package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccRotationVectorData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccRotationVectorMapperTest {

    private AbstractSensorDataMapper mapper = new NccRotationVectorMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float qW = 0.91f;
        float qX = 9.87f;
        float qY = 6.54f;
        float qZ = 3.21f;
        NccSensorData data = new NccRotationVectorData(timeUtc, qW, qX, qY, qZ);

        String expected = "Q," + qX + "," + qY + "," + qZ + "," + qW + ",NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
