package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMagneticFieldData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccMagneticFieldMapperTest {

    private AbstractSensorDataMapper mapper = new NccMagneticFieldMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float bX = 5.46f;
        float bY = 6.57f;
        float bZ = 7.68f;
        int accuracy = 50;
        NccSensorData data = new NccMagneticFieldData(timeUtc, bX, bY, bZ, accuracy);

        String expected = "M," + bX + "," + bY + "," + bZ + "," + accuracy + ",NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
