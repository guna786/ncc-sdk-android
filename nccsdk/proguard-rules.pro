# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/android-sdks/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

##############################################
#                   INTERNAL                 #
##############################################

# SDK is using GSON for conversion from JSON to POJO representation so ignore those POJO classes from being obfuscated
# Configurations
-keep class com.agero.nccsdk.**.config.** { *; }
# Other POJOs
-keep class com.agero.nccsdk.**.model.** { *; }

# Classes references by clients
-keep class com.agero.nccsdk.Ncc* { public *; }
-keep class com.agero.nccsdk.TrackingContext { public *; }
-keep class com.agero.nccsdk.domain.data.** { *; }
-keep class com.agero.nccsdk.domain.sensor.Ncc* { *; }
-keep class com.agero.nccsdk.adt.event.AdtEventListener { public *; }

# Keep so proguard does not ignore 'throws' clause on methods
-keepattributes Exceptions

##############################################
#                   EXTERNAL                 #
##############################################

### Keep enums (caused from Gson)
-keepclassmembers enum * { *; }
-keep class com.google.gson.Gson { *; }

### Fabric Proguard deobfuscation
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# AWS 2.4.4
# Class names are needed in reflection
-keepnames class com.amazonaws.**
# Request handlers defined in request.handlers
-keep class com.amazonaws.services.**.*Handler
# The following are referenced but aren't required to run
-dontwarn com.fasterxml.jackson.**
-dontwarn org.apache.commons.logging.**
# Android 6.0 release removes support for the Apache HTTP client
-dontwarn org.apache.http.**
# The SDK has several references of Apache HTTP client
-dontwarn com.amazonaws.http.**
-dontwarn com.amazonaws.metrics.**

# Duplicate definitions
-dontnote org.apache.http.params.**
-dontnote org.apache.http.conn.**
-dontnote android.net.http.**

# Google Play Services
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

# Retrofit
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions